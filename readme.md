# Tech Camp
## Coding to make your way through ideas

Welcome to the Coding course of Tech Camp. This is the repository of the course material. Here you will find the notebooks used during the morning lessons, and the material gathered during the afternoon. This page will evolve during the whole week, so stay tuned!


### Software

Of course you will need Python installed on your PC. The easiest, most complete way to install it to install `Anaconda`, a curated distribution, available for all the main platforms (Windows, Mac OS, Unix)

[Anaconda Download Page](https://www.anaconda.com/download)

